﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.EntityMapper
{
    public class MessageMap : IEntityTypeConfiguration<Message>
    {
        public void Configure(EntityTypeBuilder<Message> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_messageid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("id")
                .HasColumnType("UNIQUEIDENTIFIER");
            builder.Property(x => x.Text)
                .HasColumnName("message_content")
                .HasColumnType("NVARCHAR(255)")
                .IsRequired();
            builder.Property(x => x.CreatedAt)
                .HasColumnName("created_at")
                .HasColumnType("DATETIME");
            builder.Property(x => x.Image)
                .HasColumnName("image")
                .HasColumnType("NVARCHAR(255)")
                .IsRequired();
        }
    }
}
