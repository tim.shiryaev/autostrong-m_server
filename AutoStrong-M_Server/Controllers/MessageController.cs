﻿using Azure.Core;
using Domain.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Service.MessageService;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq.Expressions;

namespace AutoStrong_M_Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService _messageService;
        private string _dirPath;

        public MessageController(IMessageService messageService)
        {
            _messageService = messageService;
            _dirPath = AppDomain.CurrentDomain.BaseDirectory;
        }

        [HttpGet(nameof(GetMessage))]
        public IActionResult GetMessage(Guid id)
        {
            try
            {
                var result = _messageService.GetMessage(id) as Message;
                string dbPath = result.Image;

                string savePath = _dirPath + dbPath;

                byte[] imgByte = System.IO.File.ReadAllBytes(savePath);
                result.Image = Convert.ToBase64String(imgByte);

                if (result is not null)
                {
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return BadRequest("No entries found");
            }

            return BadRequest("No entries found");
        }

        [HttpGet(nameof(GetAllMessages))]
        public IActionResult GetAllMessages()
        {
            var result = _messageService.GetAllMessages().Select(x => x.Id).ToList();
            if (result is not null)
            {
                return Ok(result);
            }

            return BadRequest("No entries found");
        }

        [HttpPost(nameof(InsertMessage))]
        [Consumes("application/x-www-form-urlencoded")]
        public IActionResult InsertMessage([FromForm] Message message)
        {
            var img = Convert.FromBase64String(message.Image);
            string dbPath = $"/public/img/{Guid.NewGuid()}.png";
            
            string savePath = _dirPath + dbPath;

            try
            {
                if (img != null && img.Length > 0)
                {
                    System.IO.File.WriteAllBytes(savePath, img);
                    message.Image = dbPath;
                }
            }
            catch(DirectoryNotFoundException){
                System.IO.Directory.CreateDirectory(
                    savePath.Substring(0, savePath.LastIndexOf("/"))
                );
            }
            finally
            {
                System.IO.File.WriteAllBytes(savePath, img);
                message.Image = dbPath;
            }

            message.CreatedAt = DateTime.Now;

            _messageService.InsertMessage(message);
            return Ok("Insert success");
        }
    }
}
