﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.MessageService
{
    public interface IMessageService
    {
        IEnumerable<Message> GetAllMessages();
        Message GetMessage(Guid id);
        void InsertMessage(Message message);
    }
}
