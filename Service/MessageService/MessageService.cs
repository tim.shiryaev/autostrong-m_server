﻿using Domain.Entities;
using Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.MessageService
{
    public class MessageService : IMessageService
    {
        private IRepository<Message> _repository;

        public MessageService(IRepository<Message> repository)
        {
            _repository = repository;
        }

        public IEnumerable<Message> GetAllMessages()
        {
            return _repository.GetAll();
        }

        public Message GetMessage(Guid id)
        {
            return _repository.Get(id);
        }

        public void InsertMessage(Message message)
        {
            _repository.Insert(message);
        }
    }
}
