﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Repository.Migrations
{
    /// <inheritdoc />
    public partial class ChangeImageType : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "image",
                table: "Message",
                type: "NVARCHAR(255)",
                nullable: false,
                oldClrType: typeof(byte[]),
                oldType: "VARBINARY(Max)");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte[]>(
                name: "image",
                table: "Message",
                type: "VARBINARY(Max)",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "NVARCHAR(255)");
        }
    }
}
